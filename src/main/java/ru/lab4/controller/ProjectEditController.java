package ru.lab4.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.lab4.model.Project;
import ru.lab4.service.ProjectService;

import java.util.Optional;

@Controller
@RequestMapping("/project")
public class ProjectEditController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/edit/{projectId}")
    public ModelAndView edit(
            @PathVariable("projectId") String id
    ) {
        final Optional<Project> optional = projectService.findById(id);
        final Project project = optional.orElse(null);
        if (project == null) return new ModelAndView();
        return new ModelAndView("project-edit", "project", project);
    }

    @PostMapping("/edit")
    public String edit(
            @ModelAttribute("project") Project project
    ) {
        projectService.save(project);
        return "redirect:/projects/";
    }
}
