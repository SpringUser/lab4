package ru.lab4.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.lab4.model.Project;
import ru.lab4.service.ProjectService;

import java.util.List;

@Controller
@RequestMapping("/projects")
public class ProjectListController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping("/")
    public ModelAndView getListProject() {
        final List<Project> projects = projectService.findAll();
        return new ModelAndView("project-list", "projects", projects);
    }

    @PostMapping("/delete/{projectId}")
    public String removeProjectById(
        @PathVariable("projectId") String id
    ){
        projectService.deleteById(id);
        return "redirect:/projects/";
    }

}
