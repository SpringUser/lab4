<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<html>
<head>
    <title>ProjectTable</title>
</head>
<body>
<form:form action="/project/edit" method="post" modelAttribute="project">
    <p>ID</p>
    <form:input path="id" readonly="true"/>

    <p>NAME</p>
    <form:input path="name"/>

    <p>BEGIN</p>
    <form:input type="date" path="dateBegin"/>

    <p>END</p>
    <form:input type="date" path="dateEnd"/>

    <p>CREATED</p>
    <form:input type="date" path="created" readonly="true"/>

    <p>
        <button type="submit">SUBMIT</button>
    </p>
</form:form>

</body>
</html>
