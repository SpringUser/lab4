<%@ page import="java.util.List" %>
<%@ page import="ru.lab4.model.Project" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<html>
<head>
    <title>Projects</title>
</head>
<body>
<table border="2">
    <tr>
        <td>ID</td>
        <td>NAME</td>
        <td>begin</td>
        <td>end</td>
        <td>created</td>
        <td></td>
        <td></td>
    </tr>
    <% for (Project p : (List<Project>) request.getAttribute("projects")) { %>
    <tr>
        <td><%=p.getId()%></td>
        <td><%=p.getName()%></td>
        <td><%=p.getDateBegin()%></td>
        <td><%=p.getDateEnd()%></td>
        <td><%=p.getCreated()%></td>
        <td>
            <form action="delete/<%=p.getId()%>" method="post">
                <button type="submit">DELETE</button>
            </form>
        </td>
        <td>
            <form action="/project/edit/<%=p.getId()%>" method="get">
                <button type="submit">EDIT</button>
            </form>
        </td>
    </tr>
    <% } %>
</table>

<form action="/project/edit" method="post">
    <button type="submit">CREATE</button>
</form>

</body>
</html>
